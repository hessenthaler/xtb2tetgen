# include standard modules
import argparse
import numpy as np
import scipy as sp
import os
import sys
import meshio

# define main
def main():
    print("!======================================================================================================!")
    print("")
    print(" Simple converter script to convert between CHeart X / T / B, TetGen node / ele, Paraview vtu mesh files")
    print("")
    print(" Author: Andreas Hessenthaler")
    print("")
    print(" License: GNU LESSER GENERAL PUBLIC LICENSE (see LICENSE file)")
    print("")
    print("!======================================================================================================!")
    # initiate the command line parser
    parser = argparse.ArgumentParser()

    # add long and short arguments
    parser.add_argument("--cheart-file",        "-c",   required=True,                                                          help="set CHeart input file name (extension: X / T / B)")
    parser.add_argument("--tetgen-file",        "-g",   required=True,                                                          help="set TetGen input file name (extension: node / ele)")
    # parser.add_argument("--paraview-file",      "-p",   required=True,                                                          help="set Paraview input file name (extension: vtu)")
    parser.add_argument("--linearize",          "-l",   required=False, default=False,                  action="store_true",    help="if input topology is higher-order, set output topology to order 1")
    parser.add_argument("--cheart-to-tetgen",   "-q",   required=False, default=False,                  action="store_true",    help="sets converter to convert from CHeart files to TetGen files")
    # parser.add_argument("--cheart-to-paraview", "-r",   required=False, default=False,                  action="store_true",    help="sets converter to convert from CHeart files to Paraview files")
    parser.add_argument("--tetgen-to-cheart",   "-s",   required=False, default=False,                  action="store_true",    help="sets converter to convert from TetGen files to CHeart files")
    parser.add_argument("--verbose",            "-v",   required=False, default=False,                  action="store_true",    help="print values after parsing command line arguments")

    # read arguments from the command line
    args = parser.parse_args()

    # check arguments
    if not(args.cheart_to_tetgen) and not(args.tetgen_to_cheart):
        print(">>>ERROR: Flags --cheart-to-tetgen or --tetgen-to-cheart are required")
        sys.exit()
    if args.cheart_to_tetgen and args.tetgen_to_cheart:
        print(">>>ERROR: Please set converter script to --cheart-to-tetgen *or* --tetgen-to-cheart")
        sys.exit()

    # print values
    if args.verbose:
        print(">>>INFO:")
        print(">>>INFO: Command line arguments:")
        print(">>>INFO:")
        for arg in vars(args):
            print(">>>INFO:    ", arg, getattr(args, arg))
        print(">>>INFO:")

    # read data
    if os.path.isfile(args.cheart_file+".X"):
        x                   = np.loadtxt(args.cheart_file+".X", dtype=float, skiprows=1)
    else:
        print(">>>ERROR: File does not exist: "+args.cheart_file+".X")
    if os.path.isfile(args.cheart_file+".T"):
        t                   = np.loadtxt(args.cheart_file+".T", dtype=int, skiprows=1)
    else:
        print(">>>ERROR: File does not exist: "+args.cheart_file+".T")
    if os.path.isfile(args.cheart_file+".B"):
        b                   = np.loadtxt(args.cheart_file+".B", dtype=int, skiprows=1)
    else:
        print(">>>ERROR: File does not exist: "+args.cheart_file+".B")

    # get statistics
    nn                      = x.shape[0]
    nc                      = x.shape[1]
    en                      = t.shape[0]
    ec                      = t.shape[1]
    bn                      = b.shape[0]
    bc                      = b.shape[1] - 2

    # get element type
    if (nc == 3):
        if ((ec == 4) and (bc == 3)):
            et              = "lin-tet"
        else:
            print(">>>ERROR: Unknown element type!")
            sys.exit()
    else:
        print(">>>ERROR: Unknown element type!")
        sys.exit()

    # print statistics
    if args.verbose:
        print(">>>INFO:")
        print(">>>INFO: Number of nodes:                        "+str(nn))
        print(">>>INFO: Number of components:                   "+str(nc))
        print(">>>INFO: Number of elements:                     "+str(en))
        print(">>>INFO: Number of nodes per element:            "+str(ec))
        print(">>>INFO: Number of boundary patches:             "+str(bn))
        print(">>>INFO: Number of nodes per patch:              "+str(bc))
        print(">>>INFO:")
        print(">>>INFO: Element type detected as:               "+str(et))
        print(">>>INFO:")

    # export node / ele files
    xout                    = np.column_stack((np.array([i for i in range(1, nn+1, 1)]), x))
    tout                    = np.column_stack((np.array([i for i in range(1, en+1, 1)]), t))
    bout                    = np.column_stack((np.array([i for i in range(1, bn+1, 1)]), b[0:bn:1,1:5:1]))
    np.savetxt(args.tetgen_file+".node", xout, fmt="%i %.16f %.16f %.16f", delimiter=" ", header=str(nn)+" "+str(nc)+" 0 0", comments="# Node index, number of dimensions, number of attributes, boundary markers\n")
    np.savetxt(args.tetgen_file+".ele",  tout, fmt="%i %i %i %i %i",       delimiter=" ", header=str(en)+" "+str(ec)+" 0",   comments="# Element index, node indices, number of attributes\n")
    np.savetxt(args.tetgen_file+".face", bout, fmt="%i %i %i %i %i",       delimiter=" ", header=str(bn)+" 1",               comments="# Boundary element index, boundary attribute (boolean)\n")

    # # convert to vtu files using meshio
    # mesh                    = meshio.read(args.tetgen_file+".ele")
    # meshio.write("foo.vtk", mesh)
    # meshio.write("foo.xdmf", mesh)

if __name__== "__main__":
    main()